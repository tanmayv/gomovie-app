package in.co.gomovie.gomovieapp.di.module;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import in.co.gomovie.gomovieapp.authentication.AuthHelper;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */

@Module
public class StorageModule {
    @Provides
    @Singleton
    SharedPreferences provideSharedPrefs(Application application){
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    AuthHelper provideAuthHelper(){
        return new AuthHelper();
    }
}
