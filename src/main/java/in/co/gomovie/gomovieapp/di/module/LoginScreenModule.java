package in.co.gomovie.gomovieapp.di.module;

import dagger.Module;
import dagger.Provides;
import in.co.gomovie.gomovieapp.authentication.AuthHelper;
import in.co.gomovie.gomovieapp.contract.LoginScreenContract;
import in.co.gomovie.gomovieapp.di.PerActivity;
import in.co.gomovie.gomovieapp.presenter.LoginScreenPresenter;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
@Module
public class LoginScreenModule {
    private final LoginScreenContract.View mView;

    public LoginScreenModule(LoginScreenContract.View view) {
        this.mView = view;
    }

    @Provides
    @PerActivity
    LoginScreenContract.View provideLoginScreenView(){
        return mView;
    }

    @Provides
    @PerActivity
    LoginScreenContract.Presenter provideLoginScreenPresenter(){
        return new LoginScreenPresenter(mView, new AuthHelper());
    }
}
