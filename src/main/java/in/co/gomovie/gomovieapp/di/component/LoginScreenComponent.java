package in.co.gomovie.gomovieapp.di.component;

import dagger.Component;
import in.co.gomovie.gomovieapp.di.PerActivity;
import in.co.gomovie.gomovieapp.di.module.LoginScreenModule;
import in.co.gomovie.gomovieapp.view.activity.LoginActivity;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
@PerActivity
@Component(dependencies = StorageComponent.class, modules = LoginScreenModule.class)
public interface  LoginScreenComponent {
    void inject(LoginActivity loginActivity);
}
