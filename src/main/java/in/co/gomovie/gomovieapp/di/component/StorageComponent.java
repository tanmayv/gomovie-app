package in.co.gomovie.gomovieapp.di.component;

import javax.inject.Singleton;

import dagger.Component;
import in.co.gomovie.gomovieapp.authentication.AuthHelper;
import in.co.gomovie.gomovieapp.di.module.ApplicationModule;
import in.co.gomovie.gomovieapp.di.module.StorageModule;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
@Singleton
@Component(modules = {ApplicationModule.class, StorageModule.class})
public interface StorageComponent {
    AuthHelper authHelper();
}
