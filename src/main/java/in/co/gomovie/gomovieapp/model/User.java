package in.co.gomovie.gomovieapp.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import in.co.gomovie.gomovieapp.util.MyDatabase;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
@Table(database = MyDatabase.class)
public class User extends BaseModel{

    @PrimaryKey
    @Column
    String id;

    @Column
    String name;

    @Column
    String emailId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
