package in.co.gomovie.gomovieapp.contract;

import com.facebook.login.LoginResult;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
public class LoginScreenContract {
    public interface View{
        void showMessage(String message);
        void greetAndNavigateToHome();
    }

    public interface Presenter {
        void onLoginResult(LoginResult loginResult);
    }
}
