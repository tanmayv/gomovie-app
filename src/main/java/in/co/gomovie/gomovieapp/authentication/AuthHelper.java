package in.co.gomovie.gomovieapp.authentication;

import android.os.Bundle;
import android.util.Log;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.co.gomovie.gomovieapp.model.User;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
public class AuthHelper implements GraphRequest.GraphJSONObjectCallback{
    private static User loggedInUser;
    private AuthCallback callback;

    public AuthHelper(){
        List<User> users = SQLite.select().from(User.class)
                .queryList();
        if(users != null && users.size() > 0){
            loggedInUser = users.get(0);
        }

        Log.d("Log","AuthHelper is instantiated!");
    }

    public boolean isUserLoggedIn(){
        if(loggedInUser == null) return false;
        else return true;
    }

    public User getLoggedInUser(){
        return loggedInUser;
    }

    public void setLoggedInUserFromLoginResult(LoginResult loginResult, AuthCallback callback){
        Log.d("Log","Set Logged In User From LoginResult");
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),this);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAndWait();
        this.callback = callback;
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        Log.v("LoginActivity", response.toString());

        // Application code
        try {
            Log.d("Log","Got JSON object" + object.toString());
            String name = object.getString("name");
            String email = object.getString("email");
            String id = object.getString("id");
            User user = new User();
            user.setId(id);
            user.setName(name);
            user.setEmailId(email);
            user.save();
            loggedInUser = user;
            //callback.onComplete();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public interface AuthCallback{
        void onComplete();
    }
}
