package in.co.gomovie.gomovieapp;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import in.co.gomovie.gomovieapp.di.component.DaggerStorageComponent;
import in.co.gomovie.gomovieapp.di.component.StorageComponent;
import in.co.gomovie.gomovieapp.di.module.ApplicationModule;
import in.co.gomovie.gomovieapp.di.module.StorageModule;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
public class GomovieApplication extends Application {

    private StorageComponent mStorageComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(this);
        FlowManager.init(new FlowConfig.Builder(this).build());

        mStorageComponent = DaggerStorageComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .storageModule(new StorageModule())
                .build();

    }

    public StorageComponent getStorageComponent(){
        return mStorageComponent;
    }
}
