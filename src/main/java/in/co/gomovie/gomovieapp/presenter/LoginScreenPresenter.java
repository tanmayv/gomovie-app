package in.co.gomovie.gomovieapp.presenter;

import android.os.Bundle;
import android.util.Log;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.co.gomovie.gomovieapp.authentication.AuthHelper;
import in.co.gomovie.gomovieapp.contract.LoginScreenContract;
import in.co.gomovie.gomovieapp.model.User;

/**
 * Created by tanmayvijayvargiya on 13/12/16.
 */
public class LoginScreenPresenter implements LoginScreenContract.Presenter,  GraphRequest.GraphJSONObjectCallback{

    LoginScreenContract.View mView;
    AuthHelper authHelper;
    User loggedInUser;

    public LoginScreenPresenter(LoginScreenContract.View mView, AuthHelper authHelper) {
        this.mView = mView;

        List<User> users = SQLite.select().from(User.class)
                .queryList();
        if(users != null && users.size() > 0){
            loggedInUser = users.get(0);
            Log.d("Log","Welcome " + loggedInUser.getName());
        }

        if(authHelper == null){
            Log.d("LOG","Auth Helper is null");
        }else{
            Log.d("LOG","Auth Helper is not null");
            if(authHelper.isUserLoggedIn()){
                Log.d("User","Welcome " + authHelper.getLoggedInUser().getName());
            }
        }
    }


    @Override
    public void onLoginResult(LoginResult loginResult) {
        Log.d("Log","Calling AuthHelper to save loginResult " + loginResult.getAccessToken().toString());

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),this);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        Log.v("LoginActivity", response.toString());

        // Application code
        try {
            Log.d("Log","Got JSON object" + object.toString());
            String name = object.getString("name");
            String email = object.getString("email");
            String id = object.getString("id");
            User user = new User();
            user.setId(id);
            user.setName(name);
            user.setEmailId(email);
            user.save();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
