package in.co.gomovie.gomovieapp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import in.co.gomovie.gomovieapp.GomovieApplication;
import in.co.gomovie.gomovieapp.R;
import in.co.gomovie.gomovieapp.contract.LoginScreenContract;
import in.co.gomovie.gomovieapp.di.component.DaggerLoginScreenComponent;
import in.co.gomovie.gomovieapp.di.module.LoginScreenModule;

public class LoginActivity extends AppCompatActivity implements LoginScreenContract.View, FacebookCallback<LoginResult>{

    @Inject
    LoginScreenContract.Presenter mPresenter;
    CallbackManager cm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        DaggerLoginScreenComponent.builder()
                .storageComponent(((GomovieApplication)getApplication()).getStorageComponent())
                .loginScreenModule(new LoginScreenModule(this))
                .build()
                .inject(this);

        ButterKnife.bind(this);
        cm = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(cm,this);
    }

    @OnClick(R.id.button_facebook_login)
    public void onLoginButtonClick(View v){
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void showMessage(String message) {
        Log.d("Method","Show Message " + message);
    }

    @Override
    public void greetAndNavigateToHome() {
        Log.d("Method","Navigate To Home");
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Log.d("Method","Login Result");
        mPresenter.onLoginResult(loginResult);
    }

    @Override
    public void onCancel() {
        Log.d("Method","Login Cancel");
    }

    @Override
    public void onError(FacebookException error) {
        Log.d("Method","Login Error");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cm.onActivityResult(requestCode,resultCode,data);
    }
}
